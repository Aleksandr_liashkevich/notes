const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    extract: true,
    loaderOptions: {
      sass: {
        additionalData: '@import "@/assets/scss/bootstrap";'
      }
    }
  },
  chainWebpack: config => {
    config.module
      .rule('images')
      .set('parser', {
        dataUrlCondition: {
          maxSize: 50 * 1024 // 4KiB
        }
      })
  },
  pluginOptions: {
    svg: {
      inline: {}, // Pass options to vue-svg-loader
      data: {}, // Pass options to url-loader
      sprite: {}, // Pass options to svg-sprite-loader
      external: {} // Pass options to file-loader
    }
  }
})
