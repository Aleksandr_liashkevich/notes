import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../views/HomePage.vue'

export const routes = [
  {
    path: '/',
    name: 'home',
    component: HomePage
  },
  {
    path: '/note',
    component: () => import(/* webpackChunkName: "note" */ '../views/NotePage.vue'),
    children: [
      {
        path: 'create',
        name: 'noteCreate',
        component: () => import(/* webpackChunkName: "note" */ '../views/NotePage.vue')
      },
      {
        path: ':id',
        name: 'noteEdit',
        component: () => import(/* webpackChunkName: "note" */ '../views/NotePage.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
