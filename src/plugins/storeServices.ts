import { getCurrentInstance } from 'vue'

const $services = getCurrentInstance()?.appContext.config.globalProperties.$services
const emitter = getCurrentInstance()?.appContext.config.globalProperties.emitter

export { $services, emitter }
