export default {
  install (app, options) {
    app.config.globalProperties.$services = {
      /**
       * title
       * message
       * type
       * confirm
       *
       * @param data
       */
      confirm (data) {
        // console.log('confirm debug', app.config.globalProperties)
        if (data.title === undefined) data.title = ''
        if (data.message === undefined) data.message = ''
        if (data.type === undefined) data.type = 'success'
        if (typeof data.confirm !== 'function') data.confirm = Function
        app.config.globalProperties.emitter.emit('confirm', data)
      },
      isEqual (object1, object2) {
        const props1 = Object.getOwnPropertyNames(object1)
        const props2 = Object.getOwnPropertyNames(object2)

        if (props1.length !== props2.length) {
          return false
        }

        for (let i = 0; i < props1.length; i += 1) {
          const prop = props1[i]
          const bothAreObjects = typeof (object1[prop]) === 'object' && typeof (object2[prop]) === 'object'

          if ((!bothAreObjects && (object1[prop] !== object2[prop])) ||
            (bothAreObjects && !this.isEqual(object1[prop], object2[prop]))) {
            return false
          }
        }

        return true
      }
    }
  }
}
