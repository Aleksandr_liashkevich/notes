import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import VueSvgInlinePlugin from 'vue-svg-inline-plugin'
import mitt from 'mitt'
import services from '@/plugins/services'
const emitter = mitt()

const app = createApp(App)
  .use(store)
  .use(router)
  .use(VueSvgInlinePlugin)
  .use(services)
app.config.globalProperties.emitter = emitter
app.mount('#app')
