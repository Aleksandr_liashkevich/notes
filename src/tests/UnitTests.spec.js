/**
 * @jest-environment jsdom
 */
import { mount } from '@vue/test-utils'
import { jest } from '@jest/globals'
import router from '../router'
import store from '../store'
import services from '../plugins/services'
import mitt from 'mitt'

import HomePage from '../views/HomePage.vue'
import NotePage from '../views/NotePage.vue'
import App from '../App.vue'
const emitter = mitt()

// store.$services = app.config.globalProperties.$services
// store.$router = app.config.globalProperties.$router
// store.emitter = emitter

const mockRoute = {
  params: {
    id: 1
  }
}
const mockRouter = {
  push: jest.fn(),
  currentRoute: {
    value: {
      params: {
        id: null
      }
    }
  }
}
const mountOptions = {
  global: {
    plugins: [router, store, services],
    mocks: {
      $route: mockRoute,
      $router: mockRouter,
      emitter: emitter
    },
    directives: {
      'svg-inline': jest.fn()
    }
  }
}

test('Render home', () => {
  const wrapper = mount(HomePage, mountOptions)
  expect(wrapper.find('[data-test="add-note"]').text()).toBe('Добавить заметку')
})

const enterTitle = async (wrapper, title) => {
  const input = wrapper.find('input')
  await input.setValue(title)
  expect(input.element.value).toBe(title)
}

const saveNote = async (wrapper) => {
  // Нажатие на кнопку сохранения
  const buttonSave = wrapper.find('[data-test="save-note"]')
  await buttonSave.trigger('click')
  expect(buttonSave.text()).toContain('Добавить')
}

const addTodo = async (wrapper, name) => {
  const inputAddTodo = wrapper.find('[data-test="input-add-todo"]')
  await inputAddTodo.setValue(name)
  const buttonAddTodo = wrapper.find('[data-test="button-add-todo"]')
  await buttonAddTodo.trigger('click')
}

const checkTextInHome = async (find, text) => {
  const wrapper2 = mount(HomePage, mountOptions)
  expect(wrapper2.find(find).text()).toContain(text)
}

test('Create note simple', async () => {
  const wrapper = mount(NotePage, mountOptions)

  // Ввод текста заголовка
  await enterTitle(wrapper, 'first note')
  // Сохранение заметки
  await saveNote(wrapper)
  // Проверка текста
  await checkTextInHome('.card-header', 'first note')
})

test('Create note full', async () => {
  const wrapper = mount(NotePage, mountOptions)

  // Ввод текста заголовка
  await enterTitle(wrapper, 'second note')
  // Добавление TODOs
  await addTodo(wrapper, 'test 1')
  await addTodo(wrapper, 'test 2')
  await addTodo(wrapper, 'test 3')
  await addTodo(wrapper, 'test 4')
  await addTodo(wrapper, 'test 5')

  // Сохранение заметки
  await saveNote(wrapper)
  await checkTextInHome('.list-group:last-child .list-group-item', 'test 5')
})

test('Remove first note', async () => {
  const wrapper = mount(App, mountOptions)
  console.log('confirm debug', wrapper.vm)
  // wrapper.vm.emitter = emitter
  wrapper.vm.$store.$services = wrapper.vm.$services
  const buttonRemove = wrapper.find('[data-test="button-remove"]')
  await buttonRemove.trigger('click')
  const buttonConfirmRemove = wrapper.find('[data-test="modal-button-continue"]')
  expect(buttonConfirmRemove.text()).toContain('Да')
  await buttonConfirmRemove.trigger('click')

  console.log(wrapper.html())
  console.log(wrapper.findAll('.notes .card').length)
  // expect(wrapper.findAll('.notes .card').length)
})
