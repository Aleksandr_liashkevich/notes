import Todo from './ToDoModel'

export default interface Note {
  time: number
  title: string
  todo: Array<Todo>
}
