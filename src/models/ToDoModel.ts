export default interface Todo {
  flag: boolean
  name: string
  time: number
}
