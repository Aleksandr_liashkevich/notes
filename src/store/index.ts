import { createStore, createLogger } from 'vuex'
import notes from '@/store/modules/notes'
const debug = process.env.NODE_ENV !== 'production'

export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    notes
  },
  strict: true,
  plugins: debug ? [createLogger()] : []
})
