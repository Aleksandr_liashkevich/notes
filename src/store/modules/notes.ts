import router from '@/router'
import Note from '@/models/NoteModel'
import { $services, emitter } from '@/plugins/storeServices'

const state = () => ({
  notes: [] as Note[]
})

const getters = {
  allNotes: state => () => {
    return state.notes
  },
  noteById: state => (id) => {
    if (!id || id === -1) return null
    return JSON.parse(JSON.stringify(state.notes.find(note => note.time === parseInt(id))))
  }
}

const actions = {
  /**
   * Добавление заметки
   * @param dispatch
   * @param commit
   * @param payload
   */
  add ({ dispatch, commit }, payload) {
    commit('ADD', payload)
    dispatch('saveInLocalStorage')
  },
  /**
   * Удаление заметки по ID
   * @param dispatch
   * @param commit
   * @param payload
   * @returns {Promise<unknown>}
   */
  async removeById ({ dispatch, commit }, payload) {
    commit('REMOVE_BY_ID', payload)
    dispatch('saveInLocalStorage')
  },
  /**
   * Удаление заметки со всплывающим окном Confirm
   * @param dispatch
   * @param getters
   * @param payload
   * @returns {Promise<unknown>}
   */
  async removeWithConfirm ({ dispatch, getters }, payload) {
    const removeNote = async () => {
      console.log('confirm Remove removeNote')
      await dispatch('removeById', payload)

      router.push({ name: 'home' }).then(r => {
      })
      emitter.emit('alert', { message: 'Удалена заметка', type: 'success' })
    }

    $services.confirm({
      title: 'Подтвердите удаление',
      message: `Удалить ${getters.noteById(payload).title}?`,
      type: 'warning',
      confirm: removeNote
    })
  },
  /**
   * Сохранение заметок в локальное хранилище
   * @param state
   */
  saveInLocalStorage ({ state }) {
    localStorage.setItem('notes', JSON.stringify(state.notes))
  },
  /**
   * Загрузка заметок из локального хранилища
   * @param commit
   */
  loadFromLocalStorage ({ commit }) {
    const notes = JSON.parse(<string>localStorage.getItem('notes')) || []
    notes.forEach(note => {
      commit('ADD', note)
    })
  }
}

const mutations = {
  ADD (state, payload) {
    const index = state.notes.findIndex((note) => note.time === payload.time)
    if (index > -1) {
      state.notes[index] = payload
    } else {
      state.notes.push(payload)
    }
  },
  REMOVE_BY_ID (state, payload) {
    const index = state.notes.findIndex((note) => note.time === parseInt(payload))
    if (index > -1) {
      state.notes.splice(index, 1)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
